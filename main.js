function createNewUser () {
    let firstName = prompt ("Введите имя");
    let lastName = prompt ("Введите фамилию");
    let birthDay = prompt ("Введите дату Вашего рождения dd.mm.yyyy");

    let newUser = {
        firstName: firstName,
        lastName: lastName,
        getAge:function () {
            let now = new Date();
            let birthDayRevers = birthDay.split('.')
                                         .reverse()
                                         .join('-');
            let age = now - new Date(birthDayRevers)
            const msCoef = 31536000000;

            return Math.floor(age/msCoef)

        },

        getLogin: function () {
            return  firstName[0].toLowerCase() + lastName.toLowerCase()
        },

        getPassword: function () {
            return  firstName[0].toUpperCase() + lastName.toLowerCase() + birthDay.split('.')[2]
        },
    }

    return newUser
}

let user = createNewUser();
//console.log( user.getLogin() );
console.log( user.getAge() );
console.log(user.getPassword() );

